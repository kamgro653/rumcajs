class Bot:

    speed = 0
    angle = 0
    direction = 'S'
    direction_list = ['F', 'B', 'L', 'R', 'S']  # possible directions (Forward Back Left Right Stop)
    client_socket = 0
    bot_node = ['0', '0', '0']
    light = 0
    camera_on = False

    angle_turret = 0      # angle of turret

    intervalFlag = False  # interval Flag, to handle keyPress Event
