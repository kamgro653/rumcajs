import sys
from time import *
from threading import Thread
import socket
import Rumcajs_gui as Rmc
import Rumcajs_style_sheet as Rumcajs_sheet
import Rumcajs_bot as Bot
from PyQt5 import QtCore, QtGui, QtWidgets
import cv2

# client configuration data
HOST = '192.168.0.107'  # The server's hostname or IP address '192.168.0.107' -RPi address 192.168.43.217 transfer
PORT = 19000      # The port used by the server
client_type = 'normal'
msg_server = ['TEXT']


# client handle
def receive_thread(sending_socket, Bot):
    while True:
        try:
            msg = sending_socket.recv(1024)
            msg_text = msg.decode('utf-8')
            msg_server.insert(0, msg_text)
            get_command(msg_text)
            print("Message from server: "+msg_text)
        except:
            print("Cant get message from server")
            break


# camera
def camera_update(label):
    time_of_trans = 5
    temp = perf_counter() + time_of_trans
    is_timeout = False  # if true video end after set period of time, if false, it continues
    while temp > perf_counter() or not is_timeout:
        try:
            if camera_status:
                label.setHidden(False)
                capture = cv2.VideoCapture(
                    'http://192.168.0.107:8081/')  # (test/v1.3.1.avi) test video, ll be replaced in future with stream from local host (192.168.107:8081)
                ret, frame = capture.read()
            else:
                label.setHidden(True)
            if ret:
                image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                flipped_image = cv2.flip(image, 1)
                convert_to_qt_format = QtGui.QImage(flipped_image.data, flipped_image.shape[1], flipped_image.shape[0],
                                                    QtGui.QImage.Format_RGB888)
            else:
                label.setHidden(True)
        except:
            label.setHidden(True)
            print('There is problem with a video')


def get_command(data):   # getting command from received string
    par = data.split(' ')
    # todo add command handle


def map_distance(distance, nr):
    try:
        bar1_value = (float(distance) / 200) * 100
        if bar1_value > 100:
            bar1_value = 100
        Bot.sensors_bar_value[nr] = bar1_value
    except:
        return Bot.sensors_bar_value[nr]
    return bar1_value


# class based on QMainWindow to redefine key Events
class MyWindow(QtWidgets.QMainWindow):
    def __init__(self, Bot):
        super(MyWindow, self).__init__()
        self.setWindowTitle('Operator panel')
        self.Bot = Bot

    def keyPressEvent(self, e):
        Bot.intervalFlag = True
        if e.key() == QtCore.Qt.Key_W:
            print("W")
            clickedButton[0] = 'W'
        elif e.key() == QtCore.Qt.Key_A:
            print("A")
            clickedButton[0] = 'A'
            self.Bot.direction = 'L'
        elif e.key() == QtCore.Qt.Key_D:
            print("D")
            clickedButton[0] = 'D'
            self.Bot.direction = 'R'
        elif e.key() == QtCore.Qt.Key_S:
            print("S")
            clickedButton[0] = 'S'
        elif e.key() == QtCore.Qt.Key_C:
            print("C")
            self.Bot.speed = 0
            clickedButton[0] = ''
        else:
            clickedButton[0] = ''

    def keyReleaseEvent(self, e):
        pass
        if e.key() in {QtCore.Qt.Key_D, QtCore.Qt.Key_A}:
            clickedButton[0] = ''
            if self.Bot.speed > 0:
                Bot.direction = 'F'
            elif self.Bot.speed < 0:
                Bot.direction = 'B'
            elif self.Bot.speed == 0:
                Bot.direction = 'S'


def inc_angle(Bot):
    Bot.angle += 10
    if Bot.angle > 360:    # 0-360 range
        Bot.angle -= 360


def dec_angle(Bot):
    Bot.angle -= 10
    if Bot.angle < 0:    # 0-360 range
        Bot.angle += 360


def speed_up(Bot):
    if Bot.speed < 100:
        Bot.speed += 5


def slow_down(Bot):
    if Bot.speed > -100:
        Bot.speed -= 5


def turn_left(Bot):
    if Bot.speed != 0:
        Bot.direction = 'L'


def turn_right(Bot):
    if Bot.speed != 0:
        Bot.direction = 'R'


def release_button(Bot):
    if Bot.speed > 0:
        Bot.direction = 'F'
    elif Bot.speed > 0:
        Bot.direction = 'B'
    else:
        Bot.direction = 'S'


def stop_bot(Bot):
    Bot.speed = 0


def turn_on_buzzer(Bot):
    Bot.client_socket.sendall("Buzzer 1".encode('utf-8'))


def turn_off_buzzer(Bot):
    Bot.client_socket.sendall("Buzzer 0".encode('utf-8'))


def turn_on_off_light(Bot, gui):
    if Bot.light == 0:
        Bot.light = 1
        Bot.client_socket.sendall("Light 1".encode('utf-8'))
        Rumcajs_sheet.set_positive_light(gui.light_ind)
    else:
        Bot.light = 0
        Bot.client_socket.sendall("Light 0".encode('utf-8'))
        Rumcajs_sheet.set_negative_light(gui.light_ind)


def turn_on_off_camera(Bot, gui):
    if not Bot.camera_on:
        Bot.camera_on = True
        Bot.client_socket.sendall("Camera 1".encode('utf-8'))
        Rumcajs_sheet.set_positive_light(gui.camera_ind)
    else:
        Bot.client_socket.sendall("Camera 0".encode('utf-8'))
        Rumcajs_sheet.set_negative_light(gui.camera_ind)
        Bot.camera_on = False


def refresh(gui, Bot):      # function connected to timer
    # Bot speed, control by keys
    if Bot.speed < 100 and clickedButton[0] == 'W' and Bot.intervalFlag:
        Bot.speed += 5
        Bot.intervalFlag = False
    if Bot.speed > -100 and clickedButton[0] == 'S' and Bot.intervalFlag:
        Bot.speed -= 5
        Bot.intervalFlag = False
    # Bot direction
    if Bot.speed == 0:
        Bot.direction = 'S'
    elif Bot.speed > 0 and Bot.direction not in {'R', 'L'}:
        Bot.direction = 'F'
    elif Bot.speed < 0 and Bot.direction not in {'R', 'L'}:
        Bot.direction = 'B'

    if Bot.speed > 0:    # set progress bar appearance based on speed value
        Rumcajs_sheet.set_positive_progressbar(gui.progressBar)
    else:
        Rumcajs_sheet.set_negative_progressbar(gui.progressBar)

    Rumcajs_sheet.set_positive_progressbar(gui.progressBar_2)   # set progress bar of angle

    gui.progressBar.setValue(abs(Bot.speed))
    # send data about motor to server
    motor_data = 'Motor'+' '+Bot.direction+' '+str(Bot.speed)
    # test_data = "Info desktop"
    Bot.client_socket.sendall(motor_data.encode('utf-8'))

    # angle data and send
    angle_data = 'Angle' + ' ' + str(Bot.angle)
    Bot.client_socket.sendall(angle_data.encode('utf-8'))
    ui.angle_value.setText(str(Bot.angle))
    gui.progressBar_2.setValue(abs(Bot.angle))
    # send data about servo angle to server
    # servo_data = 'Servo' + ' ' + str(Bot.bot_node[0]) + ' ' + str(Bot.bot_node[1]) + ' ' + str(Bot.bot_node[2])
    #Bot.client_socket.sendall(servo_data.encode('utf-8'))


def sendToServer(c_socket, data):
    c_socket.sendall(data.encode('utf-8'))


def addTimer(gui, Bot):   # function to add timer to gui
    ui.timer = QtCore.QTimer()
    gui.timer.timeout.connect(lambda: refresh(gui, Bot))
    gui.timer.setInterval(100)
    gui.timer.start()


clickedButton = ['']  # define which button was clicked last time
intervalFlag = [False]  # setting after key event, resetting in refresh()
speed = [0]           # speed

if __name__ == "__main__":
    # client
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall(client_type.encode('utf-8'))  # sending type of client socket to server
    # Bot def
    Bot = Bot.Bot()
    Bot.client_socket = client_socket

    app = QtWidgets.QApplication(sys.argv)
    Rumcajs = MyWindow(Bot)
    ui = Rmc.Ui_Rumcajs()

    addTimer(ui, Bot)
    ui.setupUi(Rumcajs)

    reader_thread = Thread(target=receive_thread, args=(client_socket, Bot,))
    reader_thread.start()

    camera_thread = Thread(target=camera_update, args=(ui.camera_view,))
    camera_thread.daemon = True
    camera_thread.start()

    # Bot speed, control by buttons
    ui.SpeedButton.pressed.connect(lambda: speed_up(Bot))
    ui.SlowButton.pressed.connect(lambda: slow_down(Bot))
    ui.clockwise_button.pressed.connect(lambda: inc_angle(Bot))
    ui.counter_clockwise_button.pressed.connect(lambda: dec_angle(Bot))
    ui.LeftButton.pressed.connect(lambda: turn_left(Bot))
    ui.RighButton.pressed.connect(lambda: turn_right(Bot))
    ui.LeftButton.released.connect(lambda: release_button(Bot))
    ui.RighButton.released.connect(lambda: release_button(Bot))
    ui.StopButton.pressed.connect(lambda: stop_bot(Bot))
    ui.Light.pressed.connect(lambda: turn_on_off_light(Bot, ui))
    ui.camera_button.pressed.connect(lambda: turn_on_off_camera(Bot, ui))

    Rumcajs.show()
    sys.exit(app.exec_())