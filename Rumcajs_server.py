from time import*
import socket
import serial
import serial_rx_tx as rx_tx
import sys
import errno
from threading import Thread

# UART
port = serial.Serial('/dev/ttyACM0', 9600)   # can be ACM1 as well, if error occur check /dev
Serial_port = rx_tx.rx_tx_conn(port)
interval = 200  # in ms

# globals
# camera
camera_status = False   # default camera status - turn off

# Angle
angle_data = [0]


def serial_read():
    while True:
        Serial_port.simple_receive_data()


def serial_write():
    tmp_time = 0
    while True:
        if perf_counter() > tmp_time:
            tmp_time = perf_counter() + interval / 1000
            Serial_port.send_turret_message(angle_data[0])
        else:
            pass


serial_read_thread = Thread(target=serial_read)
serial_read_thread.daemon = True
serial_read_thread.start()


serial_write_thread = Thread(target=serial_write)
serial_write_thread.daemon = True
serial_write_thread.start()


# server configuration
HOST = '192.168.0.107'  # Standard loopback interface address / '192.168.0.107' -RPi address '127.0.0.1' - local 192.168.43.217 - transfer
PORT = 19000       # Port to listen on (non-privileged ports are > 1023)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()
client_msg = ['']
reader_socket = []


def waiting_connection():
        conn, addr = server_socket.accept()
        print('Connected by', addr)
        client_socket = conn
        data = client_socket.recv(1024)
        data_text = data.decode("utf-8")
        if data_text == 'normal':
            client_thread = Thread(target=client_handle, args=(client_socket, addr,))
            client_thread.start()
            sending_thread = Thread(target=send_client, args=(client_socket, addr,))
            sending_thread.start()
        elif data_text == 'reader':
            reader_sck = client_socket
            if len(reader_socket) != 0:
                reader_socket.clear()
            reader_socket.append(reader_sck)
        else:
            print('Wrong client type')


def send_client(client_socket, addr):
    interval_time = 0.4     # delay between sending commands
    temp_time = perf_counter() + interval_time
    while True:
        try:
            # toDo send data from server
            # msg = input("")
            if (temp_time - perf_counter()) < 0:
                temp_time = perf_counter() + interval_time
                send_command(client_socket)
            # client_msg[0] = msg
            try:
                pass        # temporary
                # broadcast_reader(reader_socket[0], 'Server', msg)
            except:
                pass
        except:
            print("This client is already disconnected, address: " + str(addr))
            break


def client_handle(client_socket, addr):
    while True:
        try:
            data = client_socket.recv(1024)
            data_text = data.decode('utf-8')
            get_command(data_text)
            print("Message from operator: "+data_text)
            try:
                broadcast_reader(reader_socket[0], 'Operator', data_text)
            except:
                pass
        except:
            print("Client (address: " + str(addr) + ") has been disconnected")
            break


def get_command(data):   # getting command from received string
    par = data.split(' ')
    if par[0] == 'Motor':
        print("Motor set to "+str(par[2])+" speed")
    elif par[0] == 'Camera':
        if par[1] == 1:
            camera_status = True
        else:
            camera_status = False

    elif par[0] == 'Angle':
        print("Angle set to "+ str(par[1]))
        angle_data[0] = par[1]

    elif par[0] == 'Buzzer':
        if par[1] == '1':
            turn_on_buzzer()
        elif par[1] == '0':
            turn_off_buzzer()


def set_speed(value):
    pass


def send_command(ssocket):     # sending data about eg distance to client
    comm = "test"
    # todo make message to gui
    ssocket.sendall(comm.encode('utf-8'))


def broadcast_reader(reader_socket, sender, msg):
    msgrd = sender + ": "+msg
    reader_socket.sendall(msgrd.encode('utf-8'))


def main():
    print("Server listening for connection...")
    while True:
        waiting_connection()


if __name__ == "__main__":
    main()