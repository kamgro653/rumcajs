import serial


class rx_tx_conn:
    
    def __init__(self, port):
        self.port = port

    send_msg = ''
    receive_msg = ''

    dict_sensors = {
        0: 'A',            # test marking the sensors -> to change in final version
        1: 'B',
        2: 'C',
        3: 'D'
    }

    dict_engines = {
        0: 'MVL',
        1: 'MVR',
        2: 'SPC',
        3: 'ROT'
    }

    def send_data(self, data, data1='', data2=''):
        if self.dev_type == 0:
            self.comp_send1_msg(data, data1, data2)
        if self.dev_type == 4:
            self.comp_send2_msg(data, data1)
    
    def receive_data(self):
        if self.dev_type == 4:
            return self.comp_receive2_msg()
        elif self.dev_type == 0:
            return self.comp_receive1_msg()

    def comp_send1_msg(self, sharp_distances, temperature, rot_angle):
        self.send_msg = ''
        self.send_msg += 'DST '
        for x in range(len(self.list_dist_sensors)):
            if self.list_dist_sensors[x] == 1:
                self.send_msg += self.dict_sensors[x] + ' ' + str(sharp_distances[x]) + ' '
            else:
                self.send_msg += self.dict_sensors[x] + ' X '
        self.send_msg += 'TEMP ' + str(temperature)
        self.send_msg += '\n'
        self.port.write(self.send_msg.encode('utf-8'))
        # self.send_msg += 'ROT ' + str(rot_angle)
        # self.send_msg += '\n'
        # self.port.write(self.send_msg.encode('utf-8'))

    def comp_receive1_msg(self):                        # same as comp_receive....2 but for some time it ll work as
        raw_msg = self.port.readline().decode('utf-8')  # separate to make easier possibly changes in future
        msg = raw_msg.rstrip("\n")
        return msg

    def comp_send2_msg(self, sp_list, led):
        self.send_msg = ''
        self.send_msg = 'ENG '
        for x in range(len(self.dict_engines)):
            if self.list_eng[x] == 1:
                self.send_msg += self.dict_engines[x] + ' ' + str(sp_list[x]) + ' '
            else:
                self.send_msg += self.dict_engines[x] + ' X '
        self.send_msg += 'LED ' + str(led)
        self.send_msg += '\n'
        self.port.write(self.send_msg.encode('utf-8'))

    def comp_receive2_msg(self):
        raw_msg = self.port.readline().decode('utf-8')
        msg = raw_msg.rstrip("\n")
        return msg

    def send_turret_message(self, value):
        self.send_msg = 'Angle ' + str(value) + '\n'
        self.port.write(self.send_msg.encode('utf-8'))

    def simple_receive_data(self):
        line = self.port.readline().decode('utf-8').rstrip()
        print(line)
