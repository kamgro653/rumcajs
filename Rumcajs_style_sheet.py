# place for style sheets for Rumcajs


def set_positive_progressbar(widget):
    widget.setStyleSheet("QProgressBar\n"
                        "{\n"
                        "    border: solid black;\n"
                        "    border-radius: 15px;\n"
                        "    background-color: rgb(47, 14, 214);\n"
                        "    \n"
                        "    color: rgb(255, 117, 249);\n"
                        "}\n"
                        "QProgressBar::chunk \n"
                        "{    \n"
                        "    \n"
                        "    background-color: qlineargradient(spread:pad, x1:0.489, y1:0, x2:0.489, y2:0.994318, stop:0.373684 rgba(255, 102, 191, 255), stop:1 rgba(133, 61, 235, 255));\n"
                        "    border-radius :15px;\n"
                        "}      ")


def set_negative_progressbar(widget):
    widget.setStyleSheet("QProgressBar\n"
                          "{\n"
                          "    border: solid black;\n"
                          "    border-radius: 15px;\n"
                          "    background-color: rgb(47, 14, 214);\n"
                          "    \n"
                          "    color: rgb(255, 117, 249);\n"
                          "}\n"
                          "QProgressBar::chunk \n"
                          "{    \n"
                          "    \n"
                          " background-color: qlineargradient(spread:pad, x1:0.489, y1:0, x2:0.489, y2:0.994318, stop:0.373684 rgba(100, 209, 201, 255), stop:1 rgba(133, 61, 235, 255));\n"
                          "    border-radius :15px;\n"
                          "}      ")


def set_zero_indicator_wheel(widget):
    widget.setStyleSheet("QFrame{\n"
                              "    \n"
                              "    background-color: rgb(112, 107, 255);\n"
                              "    border: 3px solid black;\n"
                              "}")


def set_positive_indicator_wheel(widget):
    widget.setStyleSheet("QFrame{\n"
                              "    \n"
                              "    background-color: rgb(255, 112, 236);\n"
                              "    border: 3px solid black;\n"
                              "}")


def set_negative_indicator_wheel(widget):
    widget.setStyleSheet("QFrame{\n"
                              "    \n"
                              "    background-color: rgb(34, 223, 223);\n"
                              "    border: 3px solid black;\n"
                              "}")


def set_positive_light(widget):
    widget.setStyleSheet("QFrame{\n"
                         "    \n"
                         "    background-color: rgb(255, 112, 236);\n"
                         "    border: 3px solid black;\n"
                         "}")


def set_negative_light(widget):
    widget.setStyleSheet("QFrame{\n"
                         "    \n"
                         "    background-color: rgb(112, 107, 255);\n"
                         "    border: 3px solid black;\n"
                         "}")
