void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);
    if (Serial.available() > 0) {
    String data = Serial.readStringUntil('\n');
    Serial.print("Arduino echo: ");
    Serial.println(data);
    }
    delay(200);
}
